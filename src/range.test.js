'use strict'

import should from 'should';

import { Range, PositiveRange, InvalidRangeError } from './range';
import lo from 'lodash';



describe ('Range', () => {
    describe ('#constructor', () => {
        it ('should create a new instance without errors', () => {
            const range = new Range (0, 1);
        });

        it ('should throw an InvalidRangeError error if max > min', (done) => {
            try {
                const range = new Range (1, 0);
                done ('constructor did not throw an error');
            } catch (error) {
                error.should.be.an.instanceof (InvalidRangeError);
                error.should.have.property ('message', 'min boundary is greater than max');
                error.should.have.property ('code', 'MIN_GT_MAX');
                error.should.have.property ('value', {min: 1, max: 0});
                done();
            }
        });

        it ('should initialize min and max getters properly', () => {
            const range = new Range (100, 200);

            range.min.should.be.exactly (100);
            range.max.should.be.exactly (200);
        });

        it ('should have the same min and max if only min has been passed', () => {
            const range = new Range (42);

            range.min.should.be.exactly(42);
            range.max.should.be.exactly(42);
        });
    });

    describe ('#check', () => {
        it ('should check that a numeric value within the boundaries belongs to the range', () => {
            const range = new Range (15, 17);

            const result = range.check (16);

            result.should.be.a.Boolean ();
            result.should.be.exactly (true);
        });

        it ('should include boundaries into the range', () => {
            const range = new Range (15, 17);

            const min_result = range.check (15);
            min_result.should.be.a.Boolean ();
            min_result.should.be.exactly (true);

            const max_result = range.check (17);
            max_result.should.be.a.Boolean ();
            max_result.should.be.exactly (true);
        });
    });
});


describe ('PositiveRange', () => {
    describe ('#constructor', () => {
        it ('should check that min boundary is greater than zero', (done) => {
            try {
                const range = new PositiveRange (-51);
                done ('should throw an error');
            } catch (error) {
                error.should.be.an.instanceof (InvalidRangeError);
                error.should.have.property ('message', 'min boundary should be >= 0');
                error.should.have.property ('code', 'MIN_LT_ZERO');
                error.should.have.property ('value', -51);
                done ();
            }
        });
    });
});


