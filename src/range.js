'use strict'


import Error from './error';



/**
 * Range implements a range of numeric values with
 * lower and upper boundaries included
 */
export class Range {
    /**
     * @param {int} min lowest boundary of the range
     * @param {int} max highest boundary of the range
     */
    constructor (min, max = min) {
        if (min > max) throw new InvalidRangeError ('min boundary is greater than max', 'MIN_GT_MAX', {min, max});

        this._min = min;
        this._max = max;
    }

    get min () { return this._min }
    get max () { return this._max }


    /**
     * Check whether the value belongs to the range
     *
     * @param {int} value
     * @return {bool} value
     */
    check (value) { return value >= this.min && value <= this.max }
}



export class PositiveRange extends Range {
    constructor (min, max) {
        super (min, max);
        if (this.min < 0) throw new InvalidRangeError ('min boundary should be >= 0', 'MIN_LT_ZERO', min);
    }
}


/**
 * Error thrown when Range initialization failed
 */
export class InvalidRangeError extends Error {}
