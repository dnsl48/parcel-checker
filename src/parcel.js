'use strict'


import Error from './error';


export default class Parcel {
    /**
     * @param {int} length in millimetres
     * @param {int} breadth in millimetres
     * @param {int} height in millimetres
     * @param {int} weight in gramms
     */
    constructor ({length, breadth, height, weight}) {
        if (length === undefined) throw new InvalidParcelError ('length must be defined', 'LENGTH', length);
        if (breadth === undefined) throw new InvalidParcelError ('breadth must be defined', 'BREADTH', breadth);
        if (height === undefined) throw new InvalidParcelError ('height must be defined', 'HEIGHT', height);
        if (weight === undefined) throw new InvalidParcelError ('weight must be defined', 'WEIGHT', weight);

        this._length = length;
        this._breadth = breadth;
        this._height = height;
        this._weight = weight;
    }

    get length () { return this._length }
    get breadth () { return this._breadth }
    get height () { return this._height }
    get weight () { return this._weight }
}


/**
 * Error thrown when Parcel initialization failed
 */
export class InvalidParcelError extends Error {}
