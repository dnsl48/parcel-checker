'use strict'

import should from 'should';
import Money from 'js-money';

import Parcel from './parcel';
import ParcelType, { InvalidParcelTypeError } from './parcel-type';
import { PositiveRange } from './range';


describe ('ParcelType', () => {
    describe ('#constructor', () => {
        it ('should be able to create a new type without errors', () => {
            const parcelType = new ParcelType ({
                name: 'test type',
                length: new PositiveRange (1),
                breadth: new PositiveRange (2),
                height: new PositiveRange (3),
                weight: new PositiveRange (4),
                cost: new Money (5, Money.NZD)
            });
        });

        it ('should produce an error if length is not a PositiveRange', (done) => {
            try {
                const parcelType = new ParcelType ({
                    name: 'no length',
                    length: 1,
                    breadth: new PositiveRange (2),
                    height: new PositiveRange (3),
                    weight: new PositiveRange (4),
                    cost: new Money (5, Money.NZD)
                });
                done ('incorrectly initialized ParcelType');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelTypeError);
                error.should.have.property ('message', 'length must be a positive range');
                error.should.have.property ('code', 'LENGTH');
                error.should.have.property ('value', 1);
                done ();
            }
        });

        it ('should produce an error if breadth is not a PositiveRange', (done) => {
            try {
                const parcelType = new ParcelType ({
                    name: 'no length',
                    length: new PositiveRange (1),
                    breadth: 2,
                    height: new PositiveRange (3),
                    weight: new PositiveRange (4),
                    cost: new Money (5, Money.NZD)
                });
                done ('incorrectly initialized ParcelType');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelTypeError);
                error.should.have.property ('message', 'breadth must be a positive range');
                error.should.have.property ('code', 'BREADTH');
                error.should.have.property ('value', 2);
                done ();
            }
        });

        it ('should produce an error if height is not a PositiveRange', (done) => {
            try {
                const parcelType = new ParcelType ({
                    name: 'no length',
                    length: new PositiveRange (1),
                    breadth: new PositiveRange (2),
                    height: 3,
                    weight: new PositiveRange (4),
                    cost: new Money (5, Money.NZD)
                });
                done ('incorrectly initialized ParcelType');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelTypeError);
                error.should.have.property ('message', 'height must be a positive range');
                error.should.have.property ('code', 'HEIGHT');
                error.should.have.property ('value', 3);
                done ();
            }
        });

        it ('should produce an error if weight is not a PositiveRange', (done) => {
            try {
                const parcelType = new ParcelType ({
                    name: 'no length',
                    length: new PositiveRange (1),
                    breadth: new PositiveRange (2),
                    height: new PositiveRange (3),
                    weight: 4,
                    cost: new Money (5, Money.NZD)
                });
                done ('incorrectly initialized ParcelType');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelTypeError);
                error.should.have.property ('message', 'weight must be a positive range');
                error.should.have.property ('code', 'WEIGHT');
                error.should.have.property ('value', 4);
                done ();
            }
        });

        it ('should produce an error if money is not an instance of Money', (done) => {
            try {
                const parcelType = new ParcelType ({
                    name: 'no length',
                    length: new PositiveRange (1),
                    breadth: new PositiveRange (2),
                    height: new PositiveRange (3),
                    weight: new PositiveRange (4),
                    cost: 5
                });
                done ('incorrectly initialized ParcelType');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelTypeError);
                error.should.have.property ('message', 'money must be an instance of Money');
                error.should.have.property ('code', 'COST');
                error.should.have.property ('value', 5);
                done ();
            }
        });

        it ('should initialize attributes properly', (done) => {
            const parcelType = new ParcelType ({
                name: 'test type',
                length: new PositiveRange (1),
                breadth: new PositiveRange (2),
                height: new PositiveRange (3),
                weight: new PositiveRange (4),
                cost: new Money (5, Money.NZD)
            });

            parcelType.name.should.be.exactly ('test type')

            parcelType.length.should.be.an.instanceof (PositiveRange);
            parcelType.length.min.should.be.exactly (1);
            parcelType.length.max.should.be.exactly (1);

            parcelType.breadth.should.be.an.instanceof (PositiveRange);
            parcelType.breadth.min.should.be.exactly (2);
            parcelType.breadth.max.should.be.exactly (2);

            parcelType.height.should.be.an.instanceof (PositiveRange);
            parcelType.height.min.should.be.exactly (3);
            parcelType.height.max.should.be.exactly (3);

            parcelType.weight.should.be.an.instanceof (PositiveRange);
            parcelType.weight.min.should.be.exactly (4);
            parcelType.weight.max.should.be.exactly (4);

            parcelType.cost.should.be.an.instanceof (Money);
            parcelType.cost.amount.should.be.exactly (5);
            parcelType.cost.currency.should.be.exactly (Money.NZD.code);

            done();
        });
    });

    describe ('#checkParcel', () => {
        const parcelType = new ParcelType ({
            name: 'the name',
            length: new PositiveRange (1, 100),
            breadth: new PositiveRange (1, 100),
            height: new PositiveRange (1, 100),
            weight: new PositiveRange (1, 25000),
            cost: new Money (10, Money.NZD)
        });

        it ('should be able to determine if the parcel is within all the ranges', () => {
            const parcel = new Parcel ({
                length: 100,
                breadth: 100,
                height: 100,
                weight: 5000
            });

            const result = parcelType.checkParcel (parcel);

            result.should.be.a.Boolean ();
            result.should.be.exactly (true);
        });

        it ('should be able to determine if the parcel is too long', () => {
            const parcel = new Parcel ({
                length: 101,
                breadth: 100,
                height: 100,
                weight: 5000
            });

            const result = parcelType.checkParcel (parcel);

            result.should.be.a.Boolean ();
            result.should.be.exactly (false);
        });

        it ('should be able to determine if the parcel is too broad', () => {
            const parcel = new Parcel ({
                length: 100,
                breadth: 101,
                height: 100,
                weight: 5000
            });

            const result = parcelType.checkParcel (parcel);

            result.should.be.a.Boolean ();
            result.should.be.exactly (false);
        });

        it ('should be able to determine if the parcel is too high', () => {
            const parcel = new Parcel ({
                length: 100,
                breadth: 100,
                height: 101,
                weight: 5000
            });

            const result = parcelType.checkParcel (parcel);

            result.should.be.a.Boolean ();
            result.should.be.exactly (false);
        });

        it ('should be able to determine if the parcel is too heavy', () => {
            const parcel = new Parcel ({
                length: 100,
                breadth: 100,
                height: 100,
                weight: 25001
            });

            const result = parcelType.checkParcel (parcel);

            result.should.be.a.Boolean ();
            result.should.be.exactly (false);
        });
    });
});
