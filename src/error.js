'use strict'

import Error from 'es6-error';


export default class BaseError extends Error {
    constructor (message, code, value) {
        super (message);
        this._code = code;
        this._value = value;
    }

    get code () { return this._code; }
    get value () { return this._value; }
}
