'use strict'


import Money from 'js-money';

import Error from './error';
import { PositiveRange } from './range';


export default class ParcelType {
    /**
     * @param {string} name
     * @param {PositiveRange} length
     * @param {PositiveRange} breadth
     * @param {PositiveRange} height
     * @param {int} weight gramms
     * @param {Money} cost
     */
    constructor ({name, length, breadth, height, weight, cost}) {
        if (!(length instanceof PositiveRange)) {
            throw new InvalidParcelTypeError (
                'length must be a positive range',
                'LENGTH',
                length
            );
        }

        if (!(breadth instanceof PositiveRange)) {
            throw new InvalidParcelTypeError (
                'breadth must be a positive range',
                'BREADTH',
                breadth
            );
        }

        if (!(height instanceof PositiveRange)) {
            throw new InvalidParcelTypeError (
                'height must be a positive range',
                'HEIGHT',
                height
            );
        }

        if (!(weight instanceof PositiveRange)) {
            throw new InvalidParcelTypeError (
                'weight must be a positive range',
                'WEIGHT',
                weight
            );
        }

        if (!(cost instanceof Money)) {
            throw new InvalidParcelTypeError (
                'money must be an instance of Money',
                'COST',
                cost
            );
        }

        this._name = name;
        this._length = length;
        this._breadth = breadth;
        this._height = height;
        this._weight = weight;
        this._cost = cost;
    }

    get name () { return this._name }
    get length () { return this._length }
    get breadth () { return this._breadth }
    get height () { return this._height }
    get weight () { return this._weight }
    get cost () { return this._cost }


    /**
     * Check whether the parcel belongs to the type,
     * which would mean that all its parameters
     * are within the ranges of this parcel type
     *
     * @param {Parcel} parcel
     * @return {bool}
     */
    checkParcel (parcel) {
        if (!this.length.check (parcel.length)) return false;
        if (!this.height.check (parcel.height)) return false;
        if (!this.weight.check (parcel.weight)) return false;
        if (!this.breadth.check (parcel.breadth)) return false;

        return true;
    }
}



/**
 * Error thrown when ParcelType initialization failed
 */
export class InvalidParcelTypeError extends Error {}
