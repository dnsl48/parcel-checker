'use strict'

import should from 'should';

import Parcel, { InvalidParcelError } from './parcel';


describe ('Parcel', () => {
    describe ('#constructor', () => {
        it ('should be able to create a new type without errors', () => {
            const parcel = new Parcel ({
                weight: 10,
                length: 100,
                breadth: 200,
                height: 300
            });

            parcel.length.should.be.exactly (100);
            parcel.breadth.should.be.exactly (200);
            parcel.height.should.be.exactly (300);
            parcel.weight.should.be.exactly (10);
        });

        it ('should not be able to create without weight', (done) => {
            try {
                new Parcel ({
                    length: 100,
                    breadth: 200,
                    height: 300
                });
                done ('parcel did not throw an error');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelError);
                error.should.have.property ('message', 'weight must be defined');
                error.should.have.property ('code', 'WEIGHT');
                done ();
            }
        });

        it ('should not be able to create without length', (done) => {
            try {
                new Parcel ({
                    weight: 10,
                    breadth: 200,
                    height: 300
                });
                done ('parcel did not throw an error');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelError);
                error.should.have.property ('message', 'length must be defined');
                error.should.have.property ('code', 'LENGTH');
                done ();
            }
        });

        it ('should not be able to create without breadth', (done) => {
            try {
                new Parcel ({
                    weight: 10,
                    length: 100,
                    height: 300
                });
                done ('parcel did not throw an error');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelError);
                error.should.have.property ('message', 'breadth must be defined');
                error.should.have.property ('code', 'BREADTH');
                done ();
            }
        });

        it ('should not be able to create without height', (done) => {
            try {
                new Parcel ({
                    weight: 10,
                    length: 100,
                    breadth: 200
                });
                done ('parcel did not throw an error');
            } catch (error) {
                error.should.be.an.instanceof (InvalidParcelError);
                error.should.have.property ('message', 'height must be defined');
                error.should.have.property ('code', 'HEIGHT');
                done ();
            }
        });
    });
});
