import Parcel from './parcel';
import ParcelType from './parcel-type';
import { PositiveRange } from './range';

import Money from 'js-money';
import readline from 'readline-sync';


console.log (`Hi! I'm a demo tool. Please let me know some parameters of your parcel.`);


const length = parseInt (readline.question ('Length (mm): '));
const breadth = parseInt (readline.question ('Breadth (mm): '));
const height = parseInt (readline.question ('Height (mm): '));
const weight = parseInt (readline.question ('Weight (g): '));



function checkParcel (parcel) {
    const types = [
        new ParcelType ({
            name: 'Small',
            length: new PositiveRange (1, 200),
            breadth: new PositiveRange (1, 300),
            height: new PositiveRange (1, 150),
            weight: new PositiveRange (1, 25000),
            cost: new Money (500, Money.NZD)
        }),

        new ParcelType ({
            name: 'Medium',
            length: new PositiveRange (1, 300),
            breadth: new PositiveRange (1, 400),
            height: new PositiveRange (1, 200),
            weight: new PositiveRange (1, 25000),
            cost: new Money (750, Money.NZD)
        }),

        new ParcelType ({
            name: 'Large',
            length: new PositiveRange (1, 400),
            breadth: new PositiveRange (1, 600),
            height: new PositiveRange (1, 250),
            weight: new PositiveRange (1, 25000),
            cost: new Money (850, Money.NZD)
        })
    ];

    for (let parcelType of types) {
        if (parcelType.checkParcel (parcel)) {
            console.log (`Congratulations, your parcel is of a type ${parcelType.name} and it will cost you ${parcelType.cost} ${parcelType.cost.currency}`);
            return;
        }
    }


    console.log ('Unfortunately, we are not able to move packages of that size (or weight)');
}


try {
    const parcel = new Parcel ({length, breadth, height, weight});
    checkParcel (parcel);
} catch (error) {
    console.log ('Sorry, you gave me incorrect description of your parcel: ', error.message);
}

