var path = require('path');


module.exports = {
    entry: './src/index.js',

    output: {
        filename: 'parcel-checker.js',
        path: path.resolve(__dirname, 'dst')
    },

    target: 'node',

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    }
};
