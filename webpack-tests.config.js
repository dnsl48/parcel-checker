var path = require('path');

var WebpackShellPlugin = require('webpack-shell-plugin');


module.exports = {
    entry: './src/tests.js',

    output: {
        filename: 'tests.js',
        path: path.resolve(__dirname, 'dst')
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env'],
                        plugins: [
                            ["istanbul"]
                        ]
                    }
                }
            }
        ]
    },

    plugins: [
        new WebpackShellPlugin({
            onBuildExit: "nyc mocha dst/tests.js"
        })
    ]
};
